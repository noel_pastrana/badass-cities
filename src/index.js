import React from 'react';
import ReactDOM from 'react-dom';
import Approuter from '../src/components/Approuter';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/main.scss';

const App = (
    <Approuter/>
)

ReactDOM.render( App, document.getElementById('root'));