import React, { Fragment, Component } from 'react';
import {Form, InputGroup, FormGroup} from 'react-bootstrap';

export default class AddCities extends Component {

    constructor(props) {
        super(props);
        this.handleAddCity = this.handleAddCity.bind(this);
    }

    handleAddCity(e) {
        e.preventDefault();
        const city = e.target.elements.city.value;
        this.props.handleAddCity(city);
        e.target.elements.city.value = '';
    }
    
    render() {
      return (
        <Fragment>
            <Form onSubmit={this.handleAddCity} >
                <FormGroup >
                    <InputGroup>
                        <input type="text" name="city" className="form-control"/>
                        <InputGroup.Append>
                            <button className="btn btn-primary" >Add</button>
                        </InputGroup.Append>
                    </InputGroup>
                </FormGroup>
            </Form>
        </Fragment>
      );
    }
}



