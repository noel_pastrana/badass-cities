import React, { Component, Fragment } from 'react';
import {Form, FormGroup} from 'react-bootstrap';

class SearchCities extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.props.handleSearch.bind(this);
    }

    handleChange(e) {
        e.preventDefault();
        const searchText = e.target.value;
        this.handleSearch(searchText);
    }
    
    render() {
        return (
            <Fragment>
                <Form onSubmit={e=>this.handleChange(e)}> 
                    <FormGroup>
                        <input className="form-control" type="text" onChange={this.handleChange} placeholder="Search Cities" />
                    </FormGroup>
                </Form>   
            </Fragment>
        )
    }
}

export default SearchCities;
