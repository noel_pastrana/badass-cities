import React, { Component, Fragment } from 'react';
import {Col} from 'react-bootstrap';

class ShowCities extends Component {
    
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Fragment>
                <Col xs="12">
                    {this.props.cities.filter(city=>city.includes(this.props.search)).map(filteredCity => (
                        <p>{filteredCity}</p>
                    ))}
                </Col>
            </Fragment>    
        )
    }
}

export default ShowCities;