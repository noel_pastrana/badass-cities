import React, { Fragment, Component } from 'react';
import {Form, FormGroup, Row, Col} from 'react-bootstrap';
import AddCities from './AddCities';
import ShowCities from './ShowCities';
import SearchCities from './SearchCities';

class BadassCities extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            error: undefined,
            search: '',
            cities: ['London', 'Warsaw,', 'Manila', 'Berlin', 'Moskow', 'Belgrad', 'Bogota']
        }

        this.handleSearch = this.handleSearch.bind(this);
    }
    
    handleAddCity = (city) => {
        if (!city) {
            this.setState({error:'Enter valid value to add item'});
        } else if (this.state.cities.indexOf(city) > -1) {
            this.setState({error:'This city already exists'})
        } else {
            this.setState((prevState) => ({
                cities: prevState.cities.concat(city)
            }));
        }
    }

    handleSearch(search) {
      this.setState({
          ...this.state,
          search: `${search}`
      });
    }

    render() {
        console.log(this.state);
        return (
            <Fragment>
                <div id="b-cities">
                <div className="pt-5">
                    
                    {this.state.error && <p className="text-danger">{this.state.error}</p>}
                    
                    <Row>
                        <Col xs="12" sm>
                            <AddCities handleAddCity={this.handleAddCity}/>
                        </Col>    
                        <Col xs="12" sm>
                            <SearchCities handleSearch={this.handleSearch} />
                        </Col>
                    </Row>

                    <ShowCities cities={this.state.cities} search={this.state.search}/>
                </div>
                </div>
            </Fragment>
        )
    }
}

export default BadassCities;