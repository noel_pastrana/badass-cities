import React, { Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from '../containers/Home';
import Cities from '../containers/Cities';
import Header from '../components/Header';

const Approuter = () => {
    return (
        <Fragment>
            <Router>
                <div className="container-fluid px-0">
                    <Header/>
                    <Switch>
                        <Route path="/"><Home /></Route>
                        <Route path="/cities"><Cities /></Route>
                    </Switch>
                </div>
            </Router>
        </Fragment>
    )
}

export default Approuter;


