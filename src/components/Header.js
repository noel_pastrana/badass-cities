import React from 'react';
import {Nav, Navbar} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
const Header = () => (
  <header>
    <Navbar bg="light" expand="lg">
    <Navbar.Brand href="#home">BADASS-CITIES</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
        <NavLink activeClassName="is-active" className="nav-link" to="/">Home</NavLink>
        </Nav>
    </Navbar.Collapse>
    </Navbar>
  </header>
);

export default Header;