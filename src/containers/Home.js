import React, { Fragment } from 'react';
import {Tabs, Tab } from 'react-bootstrap';
import BadassCities from '../components/BadassCities';
import BadassCities2 from '../components/BadassCities2';

const Home = () => {
    return (
        <Fragment>
           <div className="container-fluid">
                <div className="tab-wrap">
                    <Tabs defaultActiveKey="Tab1" id="uncontrolled-tab-example">
                        <Tab eventKey="Tab1" title="Tab1">
                            <BadassCities/>
                        </Tab>
                        <Tab eventKey="Tab2" title="Tab2">
                            <BadassCities2/>
                        </Tab>
                    </Tabs>
                </div>
            </div>
        </Fragment>
    )
}

export default Home;